export default {
  // se obtiene el token desde el localStorage
  loadTokenLogin ({ commit }) {
    return new Promise(resolve => {
      let token;
      try {
        token = JSON.parse(localStorage.getItem('tokenUser')) || [];
      } catch (err) {
        token = [];
      }
      commit('SET_TOKEN_LOGIN', token);
      resolve();
    });
  },
  // se obtiene el token desde el localStorage
  loadEnfermedades ({ commit }) {
    return new Promise(resolve => {
      let enfermedades;
      try {
        enfermedades = JSON.parse(localStorage.getItem('enfermedades')) || [];
      } catch (err) {
        enfermedades = [];
      }
      commit('SET_ENFERMEDADES', enfermedades);

      resolve();
    });
  },
  // se valida que el token exista y que este vigente
  // si esta vigente y esta pronto a caducar se refresca
  checkStatusLogin ({ commit, getters }) {
    return new Promise(resolve => {
      let status = false;
      if (getters.tokenUser && getters.tokenUser.infoToken && getters.tokenUser.infoToken.access_token) {
        status = true;
      }
      commit('SET_STATUS_LOGIN', status);

      resolve();
    });
  }
};
