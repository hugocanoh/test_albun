export default {
    // retorna el token de logueo
    tokenUser: state => state.tokenUser,
    loginUser: state => state.loginUser,
    // Getters de componentes
    enfermedades: state => state.enfermedades,
    viewAuxOpen: state => state.viewAuxOpen
};
