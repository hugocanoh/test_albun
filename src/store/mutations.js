const set = stateName => (state, payload) => {
    state[stateName] = payload;
};
const toggle = stateName => state => {
    state[stateName] = !state[stateName];
};

export default {
    SET_BAR_IMAGE: set('barImage'),
    SET_DRAWER: set('drawer'),
    TOGGLE_EXAMPLE: toggle('example'),
    SET_TOKEN_LOGIN: set('tokenUser'),
    SET_STATUS_LOGIN: set('loginUser'),
    SET_VIEW_AUX_OPEN: set('viewAuxOpen'),
    SET_ENFERMEDADES: set('enfermedades')
};
