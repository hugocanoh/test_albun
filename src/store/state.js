export default {
    barColor: 'rgba(246, 238, 238, .5), rgba(238, 238, 238, 1)',
    barImage: 'https:laarl.com/xx/jjj/fondo.jpg',
    drawer: false,
    // Token del Usuario con sus roles y permisos
    tokenUser: null,
    // Estado actual del usuario logueado
    loginUser: false,
    viewAuxOpen: 'index',
    enfermedades: null
};
