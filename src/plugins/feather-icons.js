import Vue from 'vue';

const icons = ['Menu', 'ChevronRight', 'Book', 'Bell', 'Activity', 'Mail', 'Settings', 'LogOut', 'Grid', 'TrendingDown'];

icons.forEach(icon => {
    const nameIcon = `${icon}Icon`;
    // convert string PascalCase to kebab-case
    const componentName = nameIcon
        .replace(/([a-z0-9])([A-Z])/g, '$1-$2')
        .replace(/([A-Z])([A-Z])(?=[a-z])/g, '$1-$2')
        .toLowerCase();

    Vue.component(componentName, require('vue-feather-icons')[nameIcon]);
});

// example <menu-icon></menu-icon>
