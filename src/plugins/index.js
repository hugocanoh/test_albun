import Vue from 'vue';

import './axios';
import './moment';
import './chartist';
import './vee-validate';
import './sweetalert2';
import './feather-icons';
import './permission-control';
import './vue-textarea-autosize';

// ************************************************
// Otras funcionalidades
Vue.prototype.$url_api = process.env.VUE_APP_ROOT_API;
