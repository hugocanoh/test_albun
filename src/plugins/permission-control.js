import Vue from 'vue';
import store from '@/store';

// Control de permisos backend spatie/laravel-permission
// Se utiliza el store para verificar los permisos o roles del usuario

const data = store.state.tokenUser;

// Validamos por permisos
Vue.prototype.$can = value => {
  if (value === '_permitted_') { return true; }
  var permissions = data.permissions === null ? [] : data.permissions;
  var _return = false;
  if (!Array.isArray(permissions)) {
    return false;
  }
  if (value.includes('|')) {
    value.split('|').forEach(item => {
      if (permissions.includes(item.trim())) {
        _return = true;
      }
    });
  } else if (value.includes('&')) {
    _return = true;
    value.split('&').forEach(item => {
      if (!permissions.includes(item.trim())) {
        _return = false;
      }
    });
  } else {
    _return = permissions.includes(value.trim());
  }
  return _return;
};

// Validamos por role
Vue.prototype.$is = value => {
  if (value === '_permitted_') { return true; }
  var roles = data.roles === null ? [] : data.roles;
  var _return = false;
  if (!Array.isArray(roles)) {
    return false;
  }
  if (value.includes('|')) {
    value.split('|').forEach(item => {
      if (roles.includes(item.trim())) {
        _return = true;
      }
    });
  } else if (value.includes('&')) {
    _return = true;
    value.split('&').forEach(item => {
      if (!roles.includes(item.trim())) {
        _return = false;
      }
    });
  } else {
    _return = roles.includes(value.trim());
  }
  return _return;
};

// Se permiten las siguientes opciones
/*
<div v-if="$can('edit post')"></div>
<div v-if="$is('super-admin')"></div>

<!-- Puedes usar el operador OR -->
<div v-if="$can('edit post | delete post | publish post')"></div>
<div v-if="$is('editor | tester | user')"></div>

<!-- puedes usar el operador AND -->
<div v-if="$can('edit post & delete post & publish post')"></div>
 <div v-if="$is('editor & tester & user')"></div>
*/
