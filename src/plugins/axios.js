import Vue from 'vue';

// Lib imports
import Axios from 'axios';
import VueAxios from 'vue-axios';
import store from '@/store';

window.axios = Axios;

// Cargamos información de logueo del usuario y cabecera de Autorización
store.dispatch('loadEnfermedades').then(() => {});
store.dispatch('loadTokenLogin').then(() => {
    const token = store.getters.tokenUser.infoToken;
    if (token) {
        Axios.defaults.headers.common.Authorization = `${token.token_type} ${token.access_token}`;
    }
});

//* ********** all request Axios ***********
Axios.interceptors.request.use(
    config => {
        console.log('Show div loading');
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

//* ********** all response Axios ***********
Axios.interceptors.response.use(
    response => {
        console.log('Hide div loading');
        return response;
    },
    error => {
        console.log('Hide div loading');
        if (error.response.status === 500) {
            console.error('Critical error: ', error);
        }
        return Promise.reject(error);
    }
);

Axios.defaults.baseURL = process.env.VUE_APP_ROOT_API;
Vue.use(VueAxios, Axios);
