import Vue from 'vue';
import Swal from 'sweetalert2';
import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2, {
    confirmButtonColor: '#ff5300',
    cancelButtonColor: '#ff7674',
    position: 'center',
    timer: 3000,
    showConfirmButton: false
});

Vue.prototype.$msgExito = msg => {
    Swal.fire({
        icon: 'success',
        title: 'Éxito',
        text: msg,
        showConfirmButton: false,
        timer: 2000
    });
};

Vue.prototype.$msgError = (msg, txtBtn) => {
    Swal.fire({
        icon: 'error',
        title: 'Error',
        text: msg,
        confirmButtonColor: 'red',
        confirmButtonText: txtBtn || 'Entendido',
        showConfirmButton: true,
        timer: undefined
    });
};
