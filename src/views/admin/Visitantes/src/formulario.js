import { mapMutations } from 'vuex';
 export default {
  data () {
    return {
      legacySystemHTML: '',
      formulario: 0,
      abreFormulario: false
    };
  },
    methods: {
      ...mapMutations({
          regresarFormulario: 'TOOGGLE_FORMOPEN'
      })
  }
  };
