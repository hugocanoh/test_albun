
export default {
  data () {
    return {
      legacySystemHTML: '',
      valid: true,
      formulario: 0,
      title: 'Ingreso / Actualización de Tarifas',
      description: '',
      value: '',
      active: '',
      active_item: [
        { value: '1', text: 'Activo' },
        { value: '0', text: 'Inactivo' }
      ],
      rules: {
        required: value => !!value || 'Requerido.',
        min: v => v.length >= 6 || 'Min 6 caracteres.'
      },
      abreFormulario: false
    };
  },
  methods: {
    cargarVista (vista) {
      this.$store.commit('SET_VIEW_AUX_OPEN', vista);
    },

    validate () {
      this.$refs.form.validate();
    },
    registrar () {
      this.validate();
      if (this.description === '' || this.value === '' || this.active === '') { return; }

      this.axios
        .post('api/tarifas', {
          description: this.description,
          value: this.value,
          active: this.active
        })
        .then(response => {
          this.$store.commit('TOOGGLE_FORMOPEN');
        })
        .catch(e => {
          this.$msgError('Datos Incorrectos');
        });
    }

  }
};
