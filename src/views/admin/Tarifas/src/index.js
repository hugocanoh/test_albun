import formulario from '../formulario.vue';

export default {
  name: 'Tarifas',
  components: {
    formulario: formulario
  },
  data: () => ({
    totalItems: 0,
    loading: true,
    options: {},
    search: '',
    headers: [
      { text: 'Descripción', value: 'description', filterable: true },
      { text: 'Valor', value: 'value', filterable: true },
      { text: 'Estado', value: 'active', filterable: true },
      { text: 'Acciones', value: 'actions', sortable: false, align: 'end' }
    ],
    items: [],
    limit: 5
  }),

  watch: {
    options: {
      handler () {
        this.getDataFromApi()
          .then(data => {
            this.items = data.items;
            this.totalItems = data.total;
          });
      },
      deep: true
    }
  },

  computed: {
    vistaAuxOpen () {
      return this.$store.getters.viewAuxOpen;
    }
  },

  created () {
    // this.initialize();
  },

  mounted () {
    this.getDataFromApi();
  },

  methods: {
    getDataFromApi () {
      this.loading = true;
      return new Promise((resolve, reject) => {
        const { sortBy, sortDesc, page, itemsPerPage } = this.options;

        console.log('this.options', this.options);

        let items = this.getDesserts();
        const total = items.length;

        if (sortBy.length === 1 && sortDesc.length === 1) {
          items = items.sort((a, b) => {
            const sortA = a[sortBy[0]];
            const sortB = b[sortBy[0]];

            if (sortDesc[0]) {
              if (sortA < sortB) return 1;
              if (sortA > sortB) return -1;
              return 0;
            } else {
              if (sortA < sortB) return -1;
              if (sortA > sortB) return 1;
              return 0;
            }
          });
        }

        if (itemsPerPage > 0) {
          items = items.slice((page - 1) * itemsPerPage, page * itemsPerPage);
        }

        setTimeout(() => {
          this.loading = false;
          resolve({
            items,
            total
          });
        }, 1000);
      });
    },
    getDesserts () {
      this.axios
      .get('api/tarifas', {})
      .then(response => {
        console.log('response', response);
      })
      .catch(e => {
        this.$msgError('Datos Incorrectos');
      });
    }
  }
/*
  methods: {
    cargarVista (vista) {
      this.$store.commit('SET_VIEW_AUX_OPEN', vista);
    },

    getDataFromApi () {
      this.loading = true;

      this.axios
        .get('api/tarifas', {
        })
        .then(response => {
          this.items = response.data[0];
          this.totalItems = 10;
          this.loading = false;
        })
        .catch(e => {
          this.$msgError('Datos Incorrectos');
        });
      /* return new Promise((resolve, reject) => {
        const { sortBy, sortDesc, page, itemsPerPage } = this.options

        let items = this.getDesserts()
        const total = items.length

        if (sortBy.length === 1 && sortDesc.length === 1) {
          items = items.sort((a, b) => {
            const sortA = a[sortBy[0]]
            const sortB = b[sortBy[0]]

            if (sortDesc[0]) {
              if (sortA < sortB) return 1
              if (sortA > sortB) return -1
              return 0
            } else {
              if (sortA < sortB) return -1
              if (sortA > sortB) return 1
              return 0
            }
          })
        }

        if (itemsPerPage > 0) {
          items = items.slice((page - 1) * itemsPerPage, page * itemsPerPage)
        }

        setTimeout(() => {
          this.loading = false
          resolve({
            items,
            total,
          })
        }, 1000)
      })
    }
  } */

};
