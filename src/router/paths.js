/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  // Dashboard
  {
    name: 'Dashboard',
    path: '/',
    view: 'Dashboard',
    folder: '/dashboard',
    meta: { requiresAuth: true }
  },
  {
    name: 'Inicio de sesión',
    path: 'login',
    view: 'Login',
    folder: '/auth',
    meta: { requiresAuth: false }
  },
  {
    name: 'Administración de usuarios',
    path: 'usuario/admin_x',
    view: 'UserAdmin',
    folder: '/dashboard/pages',
    meta: { requiresAuth: true }
  },
  {
    name: 'Pagina',
    path: 'pagina6',
    view: 'AlbunPage',
    folder: '/dashboard',
    meta: { requiresAuth: true }
  }
];
