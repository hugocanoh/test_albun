module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: 'vuetify',
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    quotes: ['error', 'single', { avoidEscape: true, allowTemplateLiterals: false }],
    'comma-dangle': ['error', 'never'],
    'template-curly-spacing': ['off'],
    semi: ['error', 'always', { omitLastInOneLineBlock: false }],
    'vue/max-attributes-per-line': ['error', {
      singleline: 8,
      multiline: {
        max: 8,
        allowFirstLine: false
      }
    }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
